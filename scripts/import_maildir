#!/usr/bin/perl -w
# Copyright (C) all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
=begin usage
Ancient script to import a Maildir into a v1 public-inbox

	# this is only if you want a v1 inbox
	export GIT_DIR=/path/to/your/repo.git
	export GIT_AUTHOR_EMAIL='list@example.com'
	export GIT_AUTHOR_NAME='list name'
	./import_maildir /path/to/maildir/

For v2 (strongly recommended), use:

	lei convert /path/to/maildir -o /path/to/v2-inbox
	# (and `lei daemon-kill' if you don't want the daemon to linger)
=cut
use v5.12;
use Date::Parse qw/str2time/;
use PublicInbox::Eml;
use PublicInbox::Git;
use PublicInbox::Import;
sub usage {
	open my $fh, '<', __FILE__;
	("Usage:\n", grep { /^=begin usage/../^=cut/ and !/^=/m } <$fh>);
}
my $dir = shift @ARGV or die usage();
my $git_dir = `git rev-parse --git-dir`;
chomp $git_dir;
foreach my $sub (qw(cur new tmp)) {
	-d "$dir/$sub" or die "$dir is not a Maildir (missing $sub)\n";
}

my @msgs;
foreach my $sub (qw(cur new)) {
	foreach my $fn (glob("$dir/$sub/*")) {
		open my $fh, '<', $fn or next;
		my $s = PublicInbox::Eml->new(do { local $/; <$fh> });
		my $date = $s->header('Date');
		my $t = eval { str2time($date) };
		defined $t or next;
		my @fn = split(m!/!, $fn);
		push @msgs, [ $t, "$sub/" . pop @fn, $date ];
	}
}

my $git = PublicInbox::Git->new($git_dir);
chomp(my $name = `git config user.name`);
chomp(my $email = `git config user.email`);
my $im = PublicInbox::Import->new($git, $name, $email);
@msgs = sort { $b->[0] <=> $a->[0] } @msgs;
while (my $ary = pop @msgs) {
	my $fn = "$dir/$ary->[1]";
	open my $fh, '<', $fn or next;
	my $mime = PublicInbox::Eml->new(do { local $/; <$fh> });
	$im->add($mime);
}
$im->done;

1;
