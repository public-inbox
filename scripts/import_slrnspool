#!/usr/bin/perl -w
# Copyright (C) all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
=begin usage
Incremental (or one-shot) importer of a slrnpull news spool.

Since the news spool can appear as an MH folder, you may also use
lei from public-inbox 2.0+ to convert it:

	lei convert mh:$SLRNPULL_ROOT/news/foo/bar -o v2:/path/to/inbox/
	# (and `lei daemon-kill' if you don't want the daemon to linger)

But if you want to use this script:

	export ORIGINAL_RECIPIENT=address@example.com
	public-inbox-init -V2 $INBOX $INBOX_DIR $HTTP_URL $ORIGINAL_RECIPIENT
	./import_slrnspool $SLRNPULL_ROOT/news/foo/bar
=cut
use v5.12;
use PublicInbox::Config;
use PublicInbox::Eml;
use PublicInbox::Import;
use PublicInbox::Git;
sub usage {
	open my $fh, '<', __FILE__;
	("Usage:\n", grep { /^=begin usage/../^=cut/ and !/^=/m } <$fh>);
}
my $exit = 0;
my $sighandler = sub { $exit = 1 };
$SIG{INT} = $sighandler;
$SIG{TERM} = $sighandler;
my $spool = shift @ARGV or die usage();
my $recipient = $ENV{ORIGINAL_RECIPIENT};
defined $recipient or die usage();
my $cfg = PublicInbox::Config->new;
my $ibx = $cfg->lookup($recipient);
my $git = $ibx->git;
my $im;
if ($ibx->version == 2) {
	require PublicInbox::V2Writable;
	$im = PublicInbox::V2Writable->new($ibx);
	$im->{parallel} = 0; # pointless to be parallel for a single message
} else {
	$im = PublicInbox::Import->new($git, $ibx->{name},
					$ibx->{-primary_address});
}

$ibx->{filter} ||= 'PublicInbox::Filter::Gmane';
my $filter = $ibx->filter;

sub key {
	"publicinbox.$ibx->{name}.importslrnspoolstate";
}

sub get_min {
	my $f = PublicInbox::Config->default_file;
	my $out = $git->qx('config', "--file=$f", key($ibx));
	$out ||= 0;
	chomp $out;
	$out =~ /\A[0-9]+\z/ and return $out;
	0;
}

sub set_min {
	my ($num) = @_;
	my $f = PublicInbox::Config->default_file;
	my @cmd = (qw/git config/, "--file=$f", key($ibx), $num);
	system(@cmd) == 0 or die join(' ', @cmd). " failed: $?\n";
}

my $n = get_min();
my $ok;
my $max_gap = 200000;
my $max = $n + $max_gap;
$spool =~ s!/+\z!!;

for (; $exit == 0 && $n < $max; $n++) {
	my $fn = "$spool/$n";
	open(my $fh, '<', $fn) or next;
	$max = $n + $max_gap;
	print STDERR $fn, "\n";

	my $mime = PublicInbox::Eml->new(do { local $/; <$fh> });
	$filter->scrub($mime);
	$im->add($mime);

	$ok = $n + 1;
	set_min($ok);
}

$im->done;
