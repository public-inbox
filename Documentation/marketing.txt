marketing guide for public-inbox + lei

TL; DR: Don't market this.

If you must: don't be pushy and annoying about it.  Slow down.
Please no superlatives, hype or BS.  Please keep all marketing
materials text-only to be accessible to those on slow networks
and ancient hardware.

It's online and public, so it already markets itself.
Being informative is not a bad thing, being insistent is.

Chances are, you're preaching to the choir; or the folks you're
trying to convince are not ready for everything our project
represents to the resistance against centralization.

Baby steps...

There's never a need for anybody to migrate to using our
software, or to use any particular instance of it.  It's
designed to coexist with other mail archives, especially
other installations of public-inbox.

Most importantly, we take victories even when our software
doesn't get adopted.  Freedom from lock-in is more important
than the adoption of any software.

Every time somebody recognizes and rejects various forms of
lock-in and centralization is already a victory for us.

Please keep in mind:

* Perl 5 is not a well-liked language
* AGPL is not a well-liked license
* maintainer is a shy introvert

Be sure to mention these things in any marketing materials
to avoid wasting time of people who hate Perl and/or AGPL.
Always add a YMMV (Your Mileage May Vary) and/or similar
disclaimers if saying anything even vaguely positive.

Replies as solutions to specific problems mentioned by others on
mailing lists (or similar discussion mediums) is acceptable;
starting new threads/topics is not.
