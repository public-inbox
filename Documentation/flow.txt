# public-inbox data flow
#
# Note: choose either "delivery tools" OR "git mirroring tools"
# for a given inboxdir.  Using them simultaneously for the
# SAME inboxdir will cause conflicts.  Of course, different
# inboxdirs may choose different means of getting mail into them.
# You may fork any inbox by starting with "git mirroring tools",
# and switching to "delivery tools".

                                                 +--------------------+
                                                 |  delivery tools:   |
                                                 |  public-inbox-mda  |
                                                 | public-inbox-watch |
                                                 | public-inbox-learn |
                                                 +--------------------+
                                                   |
                                                   |
                                                   v
+----------------------+                         +--------------------+
| git mirroring tools: |                         |                    |
| public-inbox-clone,  |                         |                    |
| public-inbox-fetch,  |  git (clone|fetch) &&   |      inboxdir      |
|      grok-pull,      |  public-inbox-index     |                    |
|   various scripts    | ----------------------> |                    |
+----------------------+                         +--------------------+
                                                   |
                                                   |
                                                   v
                                                 +--------------------+
                                                 | read-only daemons: |
                                                 | public-inbox-netd  |
                                                 | public-inbox-httpd |
                                                 | public-inbox-imapd |
                                                 | public-inbox-nntpd |
                                                 +--------------------+

# Copyright all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# This file was generated from flow.txt using Graph::Easy
