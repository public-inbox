=head1 NAME

lei-edit-search - edit saved search

=head1 SYNOPSIS

lei edit-search [OPTIONS] OUTPUT

=head1 DESCRIPTION

Invoke C<git config --edit> to edit the saved search at C<OUTPUT>,
where C<OUTPUT> was supplied for argument of C<lei q -o OUTPUT ...>
A listing of outputs is available via C<lei ls-search>.

=head1 CONTACT

Feedback welcome via plain-text mail to L<mailto:meta@public-inbox.org>

The mail archives are hosted at L<https://public-inbox.org/meta/>
and L<http://4uok3hntl7oi7b4uf4rtfwefqeexfzil2w6kgk2jn5z2f764irre7byd.onion/meta/>

=head1 COPYRIGHT

Copyright all contributors L<mailto:meta@public-inbox.org>

License: AGPL-3.0+ L<https://www.gnu.org/licenses/agpl-3.0.txt>

=head1 SEE ALSO

L<lei-ls-search(1)>, L<lei-forget-search(1)>, L<lei-up(1)>, L<lei-q(1)>
