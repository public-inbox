#!perl -w
# Copyright (C) all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
use strict; use v5.10.1; use PublicInbox::TestCommon;
use PublicInbox::Config;
use PublicInbox::Spawn qw(spawn);
require_cmd('sqlite3');
require_mods qw(DBD::SQLite psgi Xapian);
use_ok($_) for qw(Plack::Test HTTP::Request::Common);
require_ok 'PublicInbox::Msgmap';
require_ok 'PublicInbox::AltId';
require_ok 'PublicInbox::WWW';
require IO::Uncompress::Gunzip;
my ($tmpdir, $for_destroy) = tmpdir();
my $aid = 'xyz';
my $cfgpath;
my $spec = "serial:$aid:file=blah.sqlite3";
my $ibx = create_inbox 'test-altid', indexlevel => 'medium',
		altid => [ $spec ], sub {
	my ($im, $ibx) = @_;
	my $altid = PublicInbox::AltId->new($ibx, $spec, 1);
	$altid->mm_alt->mid_set(1, 'a@example.com');
	undef $altid;
	$cfgpath = "$ibx->{inboxdir}/cfg";
	open my $fh, '>', $cfgpath or BAIL_OUT "open $cfgpath: $!";
	print $fh <<EOF or BAIL_OUT $!;
[publicinbox "test"]
	inboxdir = $ibx->{inboxdir}
	address = $ibx->{-primary_address}
	altid = $spec
	url = http://example.com/test
EOF
	close $fh or BAIL_OUT $!;
	$im->add(PublicInbox::Eml->new(<<'EOF')) or BAIL_OUT;
From: a@example.com
Message-Id: <a@example.com>

EOF
};
$cfgpath //= "$ibx->{inboxdir}/cfg";
my $cfg = PublicInbox::Config->new($cfgpath);
my $www = PublicInbox::WWW->new($cfg);
my $cmpfile = "$tmpdir/cmp.sqlite3";
my $client = sub {
	my ($cb) = @_;
	my $res = $cb->(POST("/test/$aid.sql.gz"));
	is($res->code, 200, 'retrieved gzipped dump');
	IO::Uncompress::Gunzip::gunzip(\($res->content) => \(my $buf));
	pipe(my ($r, $w)) or die;
	my $cmd = ['sqlite3', $cmpfile];
	my $pid = spawn($cmd, undef, { 0 => $r });
	print $w $buf or die;
	close $w or die;
	is(waitpid($pid, 0), $pid, 'sqlite3 exited');
	is($?, 0, 'sqlite3 loaded dump');
	my $mm_cmp = PublicInbox::Msgmap->new_file($cmpfile);
	is($mm_cmp->mid_for(1), 'a@example.com', 'sqlite3 dump valid');
	$mm_cmp = undef;
	unlink $cmpfile or die;

	$res = $cb->(GET('/test/?q=xyz:1'));
	is $res->code, 200, 'altid search hit';
	$res = $cb->(GET('/test/?q=xyz:10'));
	is $res->code, 404, 'altid search miss';
	$res = $cb->(GET('/test/_/text/help/'));
	is $res->code, 200, 'altid help hit';
	like $res->content, qr/\b$aid:/, 'altid shown in help';
};
test_psgi(sub { $www->call(@_) }, $client);
SKIP: {
	my $env = { PI_CONFIG => $cfgpath, TMPDIR => $tmpdir };
	test_httpd($env, $client);
}
done_testing;
