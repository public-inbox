#!perl -w
# Copyright (C) all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
use v5.12; use PublicInbox::TestCommon;
use PublicInbox::DS qw(now);
use File::Path qw(make_path);
use_ok 'PublicInbox::DirIdle';
my ($tmpdir, $for_destroy) = tmpdir();
make_path("$tmpdir/a/b", "$tmpdir/c");
my @x;
my $cb = sub { push @x, \@_ };
my $di = PublicInbox::DirIdle->new($cb);
$di->add_watches(["$tmpdir/a", "$tmpdir/c"], 1);
$PublicInbox::DS::loop_timeout = 1000;
my $end = 3 + now;
local @PublicInbox::DS::post_loop_do = (sub { scalar(@x) == 0 && now < $end });
rmdir("$tmpdir/a/b") or xbail "rmdir $!";
PublicInbox::DS::event_loop();
if (is(scalar(@x), 1, 'got an rmdir event')) {
	is($x[0]->[0]->fullname, "$tmpdir/a/b", 'got expected fullname') and
	ok($x[0]->[0]->IN_DELETE, 'IN_DELETE set');
} else {
	check_broken_tmpfs;
	xbail explain(\@x);
}

rmdir("$tmpdir/a") or xbail "rmdir $!";
@x = ();
$end = 3 + now;
PublicInbox::DS::event_loop();
if (is(scalar(@x), 1, 'got an event after rmdir')) {
	is($x[0]->[0]->fullname, "$tmpdir/a", 'got expected fullname') and
	ok($x[0]->[0]->IN_DELETE_SELF, 'IN_DELETE_SELF set');
} else {
	check_broken_tmpfs;
	diag explain(\@x);
}
rename("$tmpdir/c", "$tmpdir/j") or xbail "rmdir $!";
@x = ();
$end = 3 + now;
PublicInbox::DS::event_loop();
is(scalar(@x), 1, 'got an event') and
	is($x[0]->[0]->fullname, "$tmpdir/c", 'got expected fullname') and
	ok($x[0]->[0]->IN_DELETE_SELF || $x[0]->[0]->IN_MOVE_SELF,
		'IN_DELETE_SELF set on move');

done_testing;
