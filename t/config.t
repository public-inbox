# Copyright (C) all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
use v5.12;
use PublicInbox::TestCommon;
use PublicInbox::Import;
use PublicInbox::IO qw(write_file);
use_ok 'PublicInbox';
ok(defined(eval('$PublicInbox::VERSION')), 'VERSION defined');
use_ok 'PublicInbox::Config';
my ($tmpdir, $for_destroy) = tmpdir();
use autodie qw(close mkdir open);
my $validate_git_behavior = $ENV{TEST_VALIDATE_GIT_BEHAVIOR};

{
	my $f = "$tmpdir/bool_config";
	write_file '>', $f, <<EOM;
[imap]
	debug
	port = 2
EOM
	my $cfg = PublicInbox::Config->git_config_dump($f);
	$validate_git_behavior and
		is(xqx([qw(git config -f), $f, qw(--bool imap.debug)]),
			"true\n", 'git handles key-only as truth');
	ok($cfg->git_bool($cfg->{'imap.debug'}), 'key-only value handled');
	is($cfg->{'imap.port'}, 2, 'normal k=v read after key-only');
}

{
	PublicInbox::Import::init_bare($tmpdir);
	my $inboxdir = "$tmpdir/new\nline";
	my @cmd = ('git', "--git-dir=$tmpdir",
		qw(config publicinbox.foo.inboxdir), $inboxdir);
	xsys_e(@cmd);

	my $tmp = PublicInbox::Config->new("$tmpdir/config");

	is($tmp->{'publicinbox.foo.inboxdir'}, $inboxdir,
		'config read correctly');
	is($tmp->{'core.bare'}, 'true', 'init used --bare repo');

	my @warn;
	local $SIG{__WARN__} = sub { push @warn, @_ };
	$tmp = PublicInbox::Config->new("$tmpdir/config");
	is($tmp->lookup_name('foo'), undef, 'reject invalid inboxdir');
	like("@warn", qr/^E:.*must not contain `\\n'/sm,
		'warned about newline');
}

{
	my $f = "examples/public-inbox-config";
	ok(-r $f, "$f is readable");

	my $cfg = PublicInbox::Config->new($f);
	is_deeply($cfg->lookup('meta@public-inbox.org'), {
		'inboxdir' => '/home/pi/meta-main.git',
		'address' => [ 'meta@public-inbox.org' ],
		'domain' => 'public-inbox.org',
		'url' => [ 'http://example.com/meta' ],
		-primary_address => 'meta@public-inbox.org',
		'name' => 'meta',
		-httpbackend_limiter => undef,
	}, "lookup matches expected output");

	is($cfg->lookup('blah@example.com'), undef,
		"non-existent lookup returns undef");

	my $test = $cfg->lookup('test@public-inbox.org');
	is_deeply($test, {
		'address' => ['try@public-inbox.org',
		              'sandbox@public-inbox.org',
			      'test@public-inbox.org'],
		-primary_address => 'try@public-inbox.org',
		'inboxdir' => '/home/pi/test-main.git',
		'domain' => 'public-inbox.org',
		'name' => 'test',
		'url' => [ 'http://example.com/test' ],
		-httpbackend_limiter => undef,
	}, "lookup matches expected output for test");
}


{
	my @altid = qw(serial:gmane:file=a serial:enamg:file=b);
	my $config = cfg_new $tmpdir, <<EOF;
[publicinbox "test"]
	address = test\@example.com
	inboxdir = /path/to/non/existent
	altid=serial:gmane:file=a
	altid=serial:enamg:file=b
EOF
	my $ibx = $config->lookup_name('test');
	is_deeply($ibx->{altid}, [ @altid ]);

	$config = cfg_new $tmpdir, <<EOF;
[publicinbox "test"]
	address = test\@example.com
	inboxdir = /path/to/non/existent
EOF
	$ibx = $config->lookup_name('test');
	is($ibx->{inboxdir}, '/path/to/non/existent', 'mainrepo still works');

	$config = cfg_new $tmpdir, <<EOF;
[publicinbox "test"]
	address = test\@example.com
	inboxdir = /path/to/non/existent
	mainrepo = /path/to/deprecated
EOF
	$ibx = $config->lookup_name('test');
	is($ibx->{inboxdir}, '/path/to/non/existent',
		'inboxdir takes precedence');
}

{
	my $cfg = cfg_new $tmpdir, <<EOF;
[publicinbox "test"]
	address = test\@example.com
	inboxdir = /path/to/non/existent
	newsgroup = inbox.test
[publicinbox]
	nntpserver = news.example.com
EOF
	my $ibx = $cfg->lookup_name('test');
	is_deeply($ibx->nntp_url({ www => { pi_cfg => $cfg }}),
		[ 'nntp://news.example.com/inbox.test' ],
		'nntp_url uses global NNTP server');

	$cfg = cfg_new $tmpdir, <<EOF;
[publicinbox "test"]
	address = test\@example.com
	inboxdir = /path/to/non/existent
	newsgroup = inbox.test
	nntpserver = news.alt.example.com
[publicinbox]
	nntpserver = news.example.com
	imapserver = imaps://mail.example.com
EOF
	$ibx = $cfg->lookup_name('test');
	is_deeply($ibx->nntp_url({ www => { pi_cfg => $cfg }}),
		[ 'nntp://news.alt.example.com/inbox.test' ],
		'nntp_url uses per-inbox NNTP server');
	is_deeply($ibx->imap_url({ www => { pi_cfg => $cfg }}),
		[ 'imaps://mail.example.com/inbox.test' ],
		'nntp_url uses per-inbox NNTP server');
}

# no obfuscate domains
{
	my $cfg = cfg_new $tmpdir, <<EOF;
[publicinbox "test"]
	address = test\@example.com
	inboxdir = /path/to/non/existent
[publicinbox "foo"]
	address = foo\@example.com
	inboxdir = /path/to/foo
[publicinbox]
	noobfuscate = public-inbox.org \@example.com z\@EXAMPLE.com
[publicinbox "test"]
	obfuscate = true
EOF
	my $ibx = $cfg->lookup_name('test');
	my $re = $ibx->{-no_obfuscate_re};
	like('meta@public-inbox.org', $re,
		'public-inbox.org address not to be obfuscated');
	like('t@example.com', $re, 'example.com address not to be obfuscated');
	unlike('t@example.comM', $re, 'example.comM address does not match');
	is_deeply($ibx->{-no_obfuscate}, {
			'test@example.com' => 1,
			'foo@example.com' => 1,
			'z@example.com' => 1,
		}, 'known addresses populated');
}

my @invalid = (
	# git rejects this because it locks refnames, but we don't have
	# this problem with inbox names:
	# 'inbox.lock',

	# git rejects these:
	'', '..', '.', 'stash@{9}', 'inbox.', '^caret', '~tilde',
	'*asterisk', 's p a c e s', ' leading-space', 'trailing-space ',
	'question?', 'colon:', '[square-brace]', "\fformfeed",
	"\0zero", "\bbackspace",

);

my %X = ("\0" => '\\0', "\b" => '\\b', "\f" => '\\f', "'" => "\\'");
my $xre = join('|', keys %X);

for my $s (@invalid) {
	my $d = $s;
	$d =~ s/($xre)/$X{$1}/g;
	ok(!PublicInbox::Config::valid_foo_name($s), "`$d' name rejected");
}

# obviously-valid examples
my @valid = qw(a a@example a@example.com);

# Rejecting more was considered, but then it dawned on me that
# people may intentionally use inbox names which are not URL-friendly
# to prevent the PSGI interface from displaying them...
# URL-unfriendly
# '<', '>', '%', '#', '?', '&', '(', ')',

# maybe these aren't so bad, they're common in Message-IDs, even:
# '!', '$', '=', '+'
push @valid, qw[bang! ca$h less< more> 1% (parens) &more eql= +plus], '#hash';
for my $s (@valid) {
	ok(PublicInbox::Config::valid_foo_name($s), "`$s' name accepted");
}

{
	my $f = "$tmpdir/ordered";
	open my $fh, '>', $f;
	my @expect;
	foreach my $i (0..3) {
		push @expect, "$i";
		print $fh <<"";
[publicinbox "$i"]
	inboxdir = /path/to/$i.git
	address = $i\@example.com

	}
	close $fh;
	my $cfg = PublicInbox::Config->new($f);
	my @result;
	$cfg->each_inbox(sub { push @result, $_[0]->{name} });
	is_deeply(\@result, \@expect);
}

{
	my $cfg = cfg_new $tmpdir, <<EOF;
[publicinbox "test1"]
	address = test\@example.com
	inboxdir = /path/to/non/existent
	coderepo = project
[publicinbox "test2"]
	address = foo\@example.com
	inboxdir = /path/to/foo
	coderepo = project
[coderepo "project"]
	dir = $tmpdir/project.git
EOF
	mkdir "$tmpdir/project.git"; # must exist for ->fill_coderepo
	my $t1 = $cfg->lookup_name('test1');
	my $t2 = $cfg->lookup_name('test2');
	ok $cfg->repo_objs($t1)->[0], 'coderepo parsed';
	is($cfg->repo_objs($t1)->[0], $cfg->repo_objs($t2)->[0],
		'inboxes share ::Git object');
}

{
	for my $t (qw(TRUE true yes on 1 +1 -1 13 0x1 0x12 0X5)) {
		is(PublicInbox::Config::git_bool($t), 1, "$t is true");
		is(xqx([qw(git -c), "test.val=$t",
			qw(config --bool test.val)]),
			"true\n", "$t matches git-config behavior");
	}
	for my $f (qw(FALSE false no off 0 +0 +000 00 0x00 0X0)) {
		is(PublicInbox::Config::git_bool($f), 0, "$f is false");
		is(xqx([qw(git -c), "test.val=$f",
			qw(config --bool test.val)]),
			"false\n", "$f matches git-config behavior");
	}
	is(PublicInbox::Config::git_bool('bogus'), undef,
		'bogus is undef');
}

SKIP: {
	# XXX wildcard match requires git 2.26+
	require_git v1.8.5, 2;
	my $cfg = cfg_new $tmpdir, <<EOF;
[imap "imap://mail.example.com"]
	pollInterval = 9
EOF
	my $url = 'imap://mail.example.com/INBOX';
	is($cfg->urlmatch('imap.pollInterval', $url), 9, 'urlmatch hit');
	is($cfg->urlmatch('imap.idleInterval', $url), undef, 'urlmatch miss');
};

my $glob2re = PublicInbox::Config->can('glob2re');
is($glob2re->('http://[::1]:1234/foo/'), undef, 'IPv6 URL not globbed');
is($glob2re->('foo'), undef, 'plain string unchanged');
is_deeply($glob2re->('[f-o]'), '[f-o]' , 'range accepted');
is_deeply($glob2re->('*'), '[^/]*?' , 'wildcard accepted');
is_deeply($glob2re->('{a,b,c}'), '(a|b|c)' , 'braces');
is_deeply($glob2re->('{,b,c}'), '(|b|c)' , 'brace with empty @ start');
is_deeply($glob2re->('{a,b,}'), '(a|b|)' , 'brace with empty @ end');
is_deeply($glob2re->('{a}'), undef, 'ungrouped brace');
is_deeply($glob2re->('{a'), undef, 'open left brace');
is_deeply($glob2re->('a}'), undef, 'open right brace');
is_deeply($glob2re->('*.[ch]'), '[^/]*?\\.[ch]', 'suffix glob');
is_deeply($glob2re->('{[a-z],9,}'), '([a-z]|9|)' , 'brace with range');
is_deeply($glob2re->('\\{a,b\\}'), undef, 'escaped brace');
is_deeply($glob2re->('\\\\{a,b}'), '\\\\\\\\(a|b)', 'fake escape brace');
is_deeply($glob2re->('**/foo'), '.*/foo', 'double asterisk start');
is_deeply($glob2re->('foo/**'), 'foo/.*', 'double asterisk end');
my $re = $glob2re->('a/**/b');
is_deeply($re, 'a(?:/.*?/|/)b', 'double asterisk middle');
like($_, qr!$re!, "a/**/b matches $_") for ('a/b', 'a/c/b', 'a/c/a/b');
unlike($_, qr!$re!, "a/**/b doesn't match $_") for ('a/ab');

{
	my $w = '';
	local $SIG{__WARN__} = sub { $w .= "@_"; };
	my $cfg = cfg_new $tmpdir, <<EOF;
[publicinbox "a"]
	address = a\@example.com
	inboxdir = $tmpdir/aa
[publicinbox "b"]
	address = b\@example.com
	inboxdir = $tmpdir/aa
EOF
	$cfg->fill_all;
	like $w, qr!`\Q$tmpdir/aa\E' used by both!, 'inboxdir conflict warned';
}

{
	my $w = '';
	local $SIG{__WARN__} = sub { $w .= "@_"; };
	my $cfg = cfg_new $tmpdir, <<EOF;
[publicinbox "a"]
	address = a\@example.com
	inboxdir = $tmpdir/a
	newsgroup = inbox.test
[publicinbox "b"]
	address = b\@example.com
	inboxdir = $tmpdir/b
	newsgroup = inbox.tesT
EOF
	$cfg->fill_all;
	like $w, qr!`inbox\.test' used by both!, 'newsgroup conflict warned';
	like $w, qr!`inbox\.tesT' lowercased!, 'upcase warned';
}

done_testing;
