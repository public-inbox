public-inbox / lei installation
-------------------------------

This is for people who want to run public-inbox on their server or
lei as a command-line tool.  Any HTTP, IMAP, NNTP, or POP3 client can
access public-inbox servers, as can git-{clone,fetch} on the HTTP(S)
endpoint.

Since our marketing sucks, ease of installation is an important goal
for this project and we only depend on distro-provided packages.

As of 2024, public-inbox is packaged by several OS distributions,
listed in alphabetical order: Debian, GNU Guix, NixOS, and Void Linux.

public-inbox is developed on Debian GNU/Linux systems and will
never depend on packages outside of the "main" component of
the "oldstable" distribution, currently Debian 11.x ("bullseye"),
but older versions of Debian remain supported (as are newer ones).

Most packages are available in other GNU/Linux distributions,
Alpine Linux, FreeBSD, NetBSD, OpenBSD, and DragonflyBSD.
CentOS 7.x users will likely want newer git and Xapian for
better performance and v2 inbox support:
https://public-inbox.org/meta/20210421151308.yz5hzkgm75klunpe@nitro.local/

As of 2.0, install/deps.perl makes it easier to install target
dependencies needed for certain features.  See install/README in
the source tree for more info.

Also see sa_config/ directories in the source tree for recommended
SpamAssassin configuration examples if using public-inbox-mda or
public-inbox-watch.

Requirements
------------

public-inbox requires a number of other packages to access its full
functionality.  The core tools are, of course:

* Git (1.8.0+, 2.6+ for writing v2 inboxes)
* Perl 5.12.0+
* DBD::SQLite (needed for IMAP, NNTP, message threading, and v2 inboxes)

To accept incoming mail into a public inbox, you'll likely want:

* MTA - postfix is recommended (for public-inbox-mda)
* SpamAssassin (spamc/spamd)   (for public-inbox-watch/public-inbox-mda)

Beyond that, there is one non-standard Perl package required:

* URI                              deb: liburi-perl
                                   pkg: p5-URI
                                   rpm: perl-URI
                                   (for HTML/Atom generation)

Where "deb" indicates package names for Debian-derived distributions,
"pkg" is for the FreeBSD package (and some other common BSDs, too),
"pkgin" for NetBSD, "apk" for Alpine Linux and "rpm" is for RPM-based
distributions (only known to work on Fedora).

Most users will likely also want the following:

- DBD::SQLite                      deb: libdbd-sqlite3-perl
                                   pkg: p5-DBD-SQLite
                                   rpm: perl-DBD-SQLite
                                   (for v2, IMAP, NNTP, or gzipped mboxes)

- Xapian(.pm) (or Search::Xapian)  deb: libsearch-xapian-perl
                                   pkg: p5-Xapian (FreeBSD, NetBSD)
                                        xapian-bindings-perl (OpenBSD)
                                   rpm: perl-Search-Xapian
                                   (required for lei; HTTP and IMAP search)

Other modules might be useful as well, depending on your use case and
preferences:

- Plack                            deb: libplack-perl
                                   pkg: p5-Plack
                                   rpm: perl-Plack, perl-Plack-Test,
                                   (for WWW interface, public-inbox-httpd(1))

- Inline::C                        deb: libinline-c-perl
                                   pkg: p5-Inline-C
                                   rpm: perl-Inline (or perl-Inline-C)
                                   (required for lei on *BSD;
                                    speeds up process spawning on Linux,
                                    see public-inbox-daemon(8))

- Email::Address::XS               deb: libemail-address-xs-perl
                                   pkg: p5-Email-Address-XS
                                   (correct parsing of tricky email
                                    addresses, phrases and comments)

- Parse::RecDescent                deb: libparse-recdescent-perl
                                   pkg: p5-Parse-RecDescent
                                   rpm: perl-ParseRecDescent
                                   (for public-inbox-imapd(1))

- Mail::IMAPClient                 deb: libmail-imapclient-perl
                                   pkg: p5-Mail-IMAPClient
                                   rpm: perl-Mail-IMAPClient
                                   (only for lei and public-inbox-watch
                                    when reading from IMAP)

- BSD::Resource                    deb: libbsd-resource-perl
                                   pkg: p5-BSD-Resource
                                   rpm: perl-BSD-Resource
                                   (only for PSGI limiters,
                                    see public-inbox-config(5))

- Plack::Middleware::ReverseProxy  deb: libplack-middleware-reverseproxy-perl
                                   pkg: p5-Plack-Middleware-ReverseProxy
                                   rpm: perl-Plack-Middleware-ReverseProxy
                                   (ensures redirects are correct when running
                                    behind nginx or Varnish)

* highlight                        deb: libhighlight-perl
                                   (for syntax highlighting with coderepo)

* xapian-tools                     deb: xapian-tools
                                   pkg: xapian-core
                                   pkgin: xapian
                                   rpm: xapian-core
                                   (for public-inbox-compact(1) and
                                    public-inbox-cindex(1))

* Xapian development files         deb: libxapian-dev
                                   pkg: xapian-core
                                   pkgin: xapian
                                   apk: xapian-core-dev
                                   rpm: xapian-core-devel / xapian14-core-libs
                                   (for public-inbox-cindex(1) and future
                                    performance enhancements)

* curl (tool)                      deb, pkg, rpm: curl
                                   (for lei HTTP(S) externals with curl and
                                    public-inbox-clone(1))

- Linux::Inotify2                  deb: liblinux-inotify2-perl
                                   rpm: perl-Linux-Inotify2
                                   (for lei, public-inbox-watch and -imapd
                                    on Linux; not required as of 2.0))

- IO::KQueue                       pkg: p5-IO-KQueue
                                   (for lei, public-inbox-watch and -imapd
                                    on *BSDs only)

- Net::Server                      deb: libnet-server-perl
                                   pkg: p5-Net-Server
                                   rpm: perl-Net-Server
                                   (for HTTP/IMAP/NNTP background daemons,
                                    not needed as systemd services or
                                    foreground servers)

The following module is typically pulled in by dependencies listed
above, so there is no need to explicitly install it:

- DBI                              deb: libdbi-perl
                                   pkg: p5-DBI
                                   rpm: perl-DBI
                                   (pulled in by DBD::SQLite)

Uncommonly needed modules (see HACKING for development-only modules):

- Socket6                          deb: libsocket6-perl
                                   pkg: p5-Socket6
                                   rpm: perl-Socket6
                                   (pulled in by SpamAssassin and Net::Server,
                                    only necessary if using IPv6 with
                                    Plack::Middleware::AccessLog or similar
                                    on Perl <= 5.12)

- Crypt::CBC                       deb: libcrypt-cbc-perl
                                   pkg: p5-Crypt-CBC
                                   (for PublicInbox::Unsubscribe (rarely used))

- Date::Parse                      deb: libtimedate-perl
                                   pkg: p5-TimeDate
                                   rpm: perl-TimeDate
                                   (for broken, mostly historical emails)

standard MakeMaker installation (Perl)
--------------------------------------

To use MakeMaker, you need to ensure ExtUtils::MakeMaker is available.
This is typically installed with Perl, but RPM-based systems will likely
need to install the `perl-ExtUtils-MakeMaker' package.

Once the dependencies are installed, you should be able to build and
install the system (into /usr/local) with:

        perl Makefile.PL
        make
        make test    # see HACKING for faster tests for hackers
        make install # root permissions may be needed

symlink-install (public-inbox.git and 1.7.0+)
---------------------------------------------

For users who lack permissions and/or wish to minimize their
installation footprint, the "symlink-install" target is available in
public-inbox.git.  The following installs symlinks to $HOME/bin
pointing to the source tree:

	perl Makefile.PL
	make symlink-install prefix=$HOME

Other installation notes
------------------------

Debian 8.x (jessie) users, use Debian 8.5 or later if using Xapian:
        https://bugs.debian.org/808610

public-inbox-* commands will never store unregeneratable data in
Xapian nor any other search database we might use; Xapian
corruption will not destroy critical data.  Note: `lei' DOES store
unregeneratable data in Xapian and SQLite.

See the public-inbox-overview(7) man page for the next steps once
the installation is complete.

The following required packages are part of the Perl standard
library.  Debian-based distros put them in "libperl5.$MINOR" or
"perl-modules-5.$MINOR"; and FreeBSD puts them in "perl5".
RPM-based distros split them out into separate packages:

* autodie                          rpm: perl-autodie
* Digest::SHA                      rpm: perl-Digest-SHA
* Data::Dumper                     rpm: perl-Data-Dumper
* IO::Compress                     rpm: perl-IO-Compress
* Sys::Syslog                      rpm: perl-Sys-Syslog
* Text::ParseWords                 rpm: perl-Text-Parsewords

Copyright
---------

Copyright all contributors <meta@public-inbox.org>
License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
