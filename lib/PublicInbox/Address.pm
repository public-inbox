# Copyright (C) all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
package PublicInbox::Address;
use v5.12;
use parent qw(Exporter);
our @EXPORT_OK = qw(pairs);

sub xs_emails {
	grep { defined } map { $_->address() } parse_email_addresses($_[0])
}

sub xs_names {
	grep { defined } map {
		my $n = $_->name;
		my $addr = $_->address;
		$n = $_->user if defined($addr) && $n eq $addr;
		$n;
	} parse_email_addresses($_[0]);
}

sub xs_pairs { # for JMAP, RFC 8621 section 4.1.2.3
	[ map { # LHS (name) may be undef if there's an address
		my @p = ($_->phrase // $_->comment, $_->address);
		# show original if totally bogus:
		$p[0] = $_->original unless defined $p[1];
		\@p;
	} parse_email_addresses($_[0]) ];
}

eval {
	require Email::Address::XS;
	Email::Address::XS->import(qw(parse_email_addresses));
	*emails = \&xs_emails;
	*names = \&xs_names;
	*pairs = \&xs_pairs;
	*objects = sub { Email::Address::XS->parse(@_) };
};

if ($@) {
	require PublicInbox::AddressPP;
	*emails = \&PublicInbox::AddressPP::emails;
	*names = \&PublicInbox::AddressPP::names;
	*pairs = \&PublicInbox::AddressPP::pairs;
	*objects = \&PublicInbox::AddressPP::objects;
}

1;
