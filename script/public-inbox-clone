#!perl -w
# Copyright (C) all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# Wrapper to git clone remote public-inboxes
use v5.12;
use Getopt::Long qw(:config gnu_getopt no_ignore_case auto_abbrev);
my $opt = {};
my $help = <<EOF; # the following should fit w/o scrolling in 80x24 term:
usage: public-inbox-clone [OPTIONS] INBOX_URL [INBOX_DIR]
       public-inbox-clone [OPTIONS] ROOT_URL [DESTINATION]

  clone remote public-inboxes or grokmirror manifests

options:

  --epoch=RANGE       range of v2 epochs to clone (e.g `2..5', `~0', `~1..')
  --torsocks VAL      whether or not to wrap git and curl commands with
                      torsocks (default: `auto')
                      Must be one of: `auto', `no' or `yes'
  --dry-run | -n      show what would be cloned without cloning
  --verbose | -v      increase verbosity (may be repeated)
    --quiet | -q      disable progress reporting
    -C DIR            chdir to specified directory

See public-inbox-clone(1) man page for --manifest, --remote-manifest,
--objstore, --project-list, --post-update-hook, --include, --exclude,
--prune, --keep-going, --jobs, --inbox-config
EOF

# cgit calls it `project-list', grokmirror calls it `projectslist',
# support both :/
GetOptions($opt, qw(help|h quiet|q verbose|v+ C=s@ c=s@ include|I=s@ exclude=s@
	inbox-config=s inbox-version=i objstore=s manifest=s
	remote-manifest=s project-list|projectslist=s post-update-hook=s@
	prune|p keep-going|k exit-code purge
	dry-run|n jobs|j=i no-torsocks torsocks=s epoch=s)) or die $help;
if ($opt->{help}) { print $help; exit };
require PublicInbox::Admin; # loads Config
PublicInbox::Admin::do_chdir(delete $opt->{C});
PublicInbox::Admin::setup_signals();
$SIG{PIPE} = 'IGNORE';

my ($url, $dst, $extra) = @ARGV;
die $help if !defined($url) || defined($extra);
defined($dst) or ($dst) = ($url =~ m!/([^/]+)/?\z!);
index($dst, "\n") >= 0 and die "`\\n' not allowed in `$dst'";

# n.b. this is still a truckload of code...
require File::Spec;
require PublicInbox::LEI;
require PublicInbox::LeiExternal;
require PublicInbox::LeiMirror;

$url = PublicInbox::LeiExternal::ext_canonicalize($url);
my $lei = bless {
	env => \%ENV, opt => $opt, cmd => 'public-inbox-clone',
	0 => \*STDIN, 2 => \*STDERR,
}, 'PublicInbox::LEI';
open $lei->{1}, '+<&=', 1 or die "dup: $!";
open $lei->{3}, '.' or die "open . $!";
my $mrr = bless {
	lei => $lei,
	src => $url,
	dst => File::Spec->canonpath($dst),
}, 'PublicInbox::LeiMirror';

$? = 0;
$mrr->do_mirror;
$mrr->can('_wq_done_wait')->($$, $mrr, $lei);
exit(($lei->{child_error} // 0) >> 8);
