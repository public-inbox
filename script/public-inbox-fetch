#!perl -w
# Copyright (C) all contributors <meta@public-inbox.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# Wrapper to git fetch remote public-inboxes
use v5.12;
use Getopt::Long qw(:config gnu_getopt no_ignore_case auto_abbrev);
my $opt = {};
my $help = <<EOF; # the following should fit w/o scrolling in 80x24 term:
usage: public-inbox-fetch -C DESTINATION

  fetch remote public-inboxes

options:

  --torsocks VAL      whether or not to wrap git and curl commands with
                      torsocks (default: `auto')
                      Must be one of: `auto', `no' or `yes'
  -T NAME             Name of remote(s) to try (may be repeated)
                      default: `origin' and `_grokmirror'
  --exit-code         exit with 127 if no updates
  --verbose | -v      increase verbosity (may be repeated)
    --quiet | -q      increase verbosity (may be repeated)
    -C DIR            chdir to specified directory
EOF
GetOptions($opt, qw(help|h quiet|q verbose|v+ C=s@ c=s@ try-remote|T=s@
	prune|p
	no-torsocks torsocks=s exit-code)) or die $help;
if ($opt->{help}) { print $help; exit };
require PublicInbox::Fetch; # loads Admin
PublicInbox::Admin::do_chdir(delete $opt->{C});
PublicInbox::Admin::setup_signals();
$SIG{PIPE} = 'IGNORE';

my $lei = bless {
	env => \%ENV, opt => $opt, cmd => 'public-inbox-fetch',
	0 => \*STDIN, 1 => \*STDOUT, 2 => \*STDERR,
}, 'PublicInbox::LEI';
PublicInbox::Fetch->do_fetch($lei, '.');
exit(($lei->{child_error} // 0) >> 8);
